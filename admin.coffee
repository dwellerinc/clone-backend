User = require './models/user'
express = require 'express'
moment = require 'moment'
Payout = require './models/payout'
util = require './util'
_ = require 'underscore'
{ Metrics, ShortURL } = require 'short'
config = require './config'
Promise = require 'bluebird'

adminRoutes = express.Router()

adminRoutes.use (req, res, next) ->
  user = req.user
  if user.admin
    next()
  else
    res.status(403).json {success: false, message: 'No Admin Permissions'}

adminRoutes.post '/confirmPayouts', (req, res) ->
  payouts = req.body.payouts
  userIds = _.map payouts, (payout) ->
    payout._id

  payoutMap = _.reduce payouts, (map, payout) ->
    map[payout._id] = payout
    return map
  , {}

  User.find(
    _id: {$in: userIds}
  ).then (users) ->
    promises = []
    totalPayout = 0
    for user in users
      payoutData = payoutMap[user._id.toString()]
      totalPayout += payoutData.payout
      payout = new Payout(
        amount: payoutData.payout
        currency: 'dollars'
        user: user
        candidate: user.candidate
      )

      promise = payout.save().then (payout) ->
        payout.user.payouts.push(payout)
        if not payout.candidate
          payout.user.balance += payout.amount
        payout.user.save()

      promises.push(promise)

    payout = new Payout(
      amount: totalPayout
      currency: 'dollars'
    )
    promises.push(payout.save())

    Promise.all(promises).then ->
      res.json {success: true}


adminRoutes.get '/pendingPayouts', (req, res) ->

  midnightToday = moment.utc().clone().startOf('day')
  #User.find({lastAdActivityDate: {$ltk
  util.getLastPayout().then (payout) ->
    console.log payout
    lastPayoutDate = payout?.date
    query = {links: {$exists:true, $ne:[]}}
    console.log lastPayoutDate
    if lastPayoutDate
      currentPayoutStart = moment(lastPayoutDate).startOf('day').toDate()
      currentPayoutEnd = moment().startOf('day').toDate()
      query.lastAdActivityDate = {$gte: currentPayoutStart}

    pendingPayoutUsers = User.find(
      query
    ).then (users) ->
      query = {user: {$in: users}}
      if lastPayoutDate
        query.timestamp = {
          $gte: currentPayoutStart
          $lt: currentPayoutEnd
        }

      console.log query

      Metrics.find(query).then (metrics) ->
        ShortURL.find(
          dailyMetrics: {$in: metrics}
        ).then (links) ->
          linkMap = _.reduce links, (map, link) ->
            map[link._id] = link
            return map
          , {}

          userMap = _.reduce users, (map, user) ->
            map[user._id] = user
            return map
          , {}

          for metric in metrics
            link = linkMap[metric.link]

            if not link.periodMetrics
                link.periodMetrics = {clicks: 0, impressions: 0, destinationClicks: 0}

            found = false
            for met in link.dailyMetrics
              if met.toString() == metric._id.toString()
                found = true

            if found == true
              link.periodMetrics.clicks += metric.clicks
              link.periodMetrics.impressions += metric.impressions
              link.periodMetrics.destinationClicks += metric.destinationClicks

          links = _.map linkMap, (link, index) ->
            return {
              metrics: link.periodMetrics
              user: link.user
              originalUrl: link.URL
              recommendedPayout: link.periodMetrics.impressions / 1000.0 * config.dollarsPerKImpressions
            }

          for link in links
            user = userMap[link.user]
            user.pendingLinks ?= []
            user.pendingLinks.push(link)

          users = _.map userMap, (user, index) ->
            return {
              email: user.email
              links: user.pendingLinks
              candidate: user.candidate or 'None'
              _id: user._id
            }

          res.json {users: users}
    , (err) ->
      console.log err

module.exports = adminRoutes

