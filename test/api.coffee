app = require '../server'
request = require 'supertest-as-promised'
helpers = require './helpers'
short = require 'short'
User = require '../models/user'
{ ShortURL } = require 'short'

describe 'api', ->
  beforeEach  ->
    ShortURL.remove({}).then(=>
      helpers.getToken().then ({token, user}) =>
        @url = 'http://www.link.com' + Date.now()
        @token = token
        @user = user
    )

  describe '#generateLink', ->
    it 'generates a shortened url', ->
      link = 'http://google.com'
      return request(app).post('/api/generateLink').send(
        originalUrl: link
        token: @token
      ).expect(200).then (res) =>
        expect(res.body.shortUrl).to.exist
        expect(res.body.originalUrl).to.equal link
        short.retrieve(res.body.hash).then (result) =>
          expect(result.user.toString()).to.equal @user._id.toString()
        , (error) ->
          console.log error
        console.log

    it 'adds the link to the user', ->
      url = 'http://google.com'
      return request(app).post('/api/generateLink').send(
        originalUrl: url
        token: @token
      ).then (res) =>
        User.findOne({ _id: @user._id }).then (user) =>
          expect(user.links.length).to.equal 1
          ShortURL.findOne({_id: user.links[0]}).then (shortUrl) =>
            expect(shortUrl.URL).to.equal url

    it 'generates same hash for same user/link', ->
      url = 'http://google.com'
      request(app).post('/api/generateLink').send(
        originalUrl: url
        token: @token
      ).then((res) =>
        hashed = res.body.shortUrl
        return hashed
      ).then (hashed) =>
        request(app).post('/api/generateLink').send(
          originalUrl: url
          token: @token
        ).then (res) =>
          expect(res.body.shortUrl).to.equal hashed

    it 'generates different hash for different user', ->
      url = 'http://google.com'
      helpers.getToken().then(({token, user}) =>
        request(app).post('/api/generateLink').send(
          originalUrl: url
          token: token
        ).then((res) =>
          return res.body.shortUrl
        ).then((firstHash) =>
          request(app).post('/api/generateLink').send(
            originalUrl: url
            token: @token
          ).then (res) =>
            expect(res.body.shortUrl).to.not.equal firstHash
        )
      )

  describe '#getLinks', ->
    beforeEach (done) ->
      short.generate(
        URL: @url
        user: @user
      ).then (link) =>
        User.update(
          { _id: @user._id }
          { $addToSet: {links: link._id} }
        ).then (user) =>
          @link = link
          done()

    it 'returns a single link', ->
      return request(app).get('/api/getLinks').send(
        token: @token
      ).expect(200).then (res) =>
        expect(res.body.links.length).to.eq 1


