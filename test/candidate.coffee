User = require '../models/user'
helpers = require './helpers'
request = require 'supertest-as-promised'
app = require '../server'

describe 'user', ->
  before ->
    User.remove({})
    @sandbox = sinon.sandbox.create()
    helpers.getToken().then(({token, user}) =>
      @user = user
      @token = token
    )

  after: ->
    @sandbox.restore()

  describe '#setCandidate', ->
    it 'sets the candidate', ->
      request(app).post('/api/user/setCandidate').send(
        candidate: 'trump'
        token:@token
      ).then((res) =>
        expect(res.body.success).to.be.true
      ).then( =>
        User.findOne({_id:@user._id})
      ).then((user) =>
        expect(user.candidate).to.eq 'trump'
      )

    it 'doesnt set invalid candidate', ->
      @user.candidate = null
      @user.save().then( =>
        request(app).post('/api/user/setCandidate').send(
          candidate: 'supreme'
          token:@token
        )
      ).then((res) =>
        expect(res.body.success).to.be.false
      ).then( =>
        User.findOne({_id:@user._id})
      ).then((user) =>
        expect(user.candidate).to.eq null
      )




