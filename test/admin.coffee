app = require '../server'
request = require 'supertest-as-promised'
helpers = require './helpers'
short = require 'short'
User = require '../models/user'
{ ShortURL, Metrics } = require 'short'
moment = require 'moment'
Payout = require '../models/payout'

describe 'admin', ->
  before (done) ->
    User.remove({}).then =>
      helpers.getToken().then ({token, user}) =>
        @admin = user
        @token = token
        User.update({_id:user._id}, {admin: true}).then (result) =>
          helpers.getToken().then(({token, user}) =>
            @user = user
            helpers.getToken().then(({user, token}) =>
              @user2 = user
              return user
            )
          ).then =>
            done()

  describe 'admin authorization', ->
    before (done) ->
      helpers.getToken().then ({token, user}) =>
        @nonAdminToken = token
        @nonAdminUser = user
        done()

    it 'returns a 403 for non admin users', (done) ->
      request(app).get('/api/admin/pendingPayouts').send(
        token: @nonAdminToken
      ).expect(403).end (err, res) =>
        expect(res.status).to.equal 403
        done()

  describe 'pendingPayouts', ->
    before (done) ->
      Payout.remove({}).then =>
        payout = new Payout(
          date: moment().subtract(3, 'days')
        )
        payout.save().then (payout) =>
          done()


    describe 'metric1', ->
      before ->
        timestamp = moment().subtract(1, 'days').startOf('day')
        short.generate(
          URL: 'www.google.com/' + Date.now()
          user: @user
        ).then (link) =>
          @link = link
          new Metrics(
            clicks: 5
            impressions: 10
            destinationClicks: 5
            timestamp: timestamp
            user: @user
            link: @link
          ).save().then((metrics) =>
            link.dailyMetrics.push metrics
            link.save()
          ).then((link) =>
            User.update({_id:@user._id}, {
              $push: {links:link}
              lastAdActivityDate: timestamp
            }).then (result) =>
          )

      it 'returns a single user with one link with single day', (done) ->
        request(app).get('/api/admin/pendingPayouts').send(
          token: @token
        ).expect(200).end (err, res) =>
          response = res.body
          expect(response.users.length).to.eq 1
          user1 = response.users[0]
          expect(user1.links.length).to.eq 1
          expect(user1.links[0].metrics.impressions).to.eq 10
          expect(user1.links[0].metrics.clicks).to.eq 5
          expect(user1.links[0].metrics.destinationClicks).to.eq 5
          done()

      describe 'metric out of date range', ->
        before (done) ->
          timestamp = moment().subtract(20, 'days').startOf('day')
          new Metrics(
            clicks: 5
            impressions: 10
            destinationClicks: 10
            timestamp: timestamp
            user: @user
            link: @link
          ).save().then((metric) =>
            @link.dailyMetrics.push metric
            @link.save()
          ).then((link) =>
            done()
          )

        it 'doesnt return out of range metric', (done) ->
          request(app).get('/api/admin/pendingPayouts').send(
            token: @token
          ).expect(200).end (err, res) =>
            response = res.body
            expect(response.users.length).to.eq 1
            user1 = response.users[0]
            expect(user1.links.length).to.eq 1
            expect(user1.links[0].metrics.impressions).to.eq 10
            expect(user1.links[0].metrics.clicks).to.eq 5
            expect(user1.links[0].metrics.destinationClicks).to.eq 5
            done()

      describe 'hourly metric', ->
        before (done) ->
          timestamp = moment().subtract(1, 'days').startOf('hour')
          new Metrics(
            clicks: 5
            impressions: 10
            destinationClicks: 10
            timestamp: timestamp
            user: @user
            link: @link
          ).save().then((metric) =>
            @link.hourlyMetrics.push metric
            @link.save()
          ).then((link) =>
            done()
          )

        it 'doesnt return hourly metrics', (done) ->
          request(app).get('/api/admin/pendingPayouts').send(
            token: @token
          ).expect(200).end (err, res) =>
            response = res.body
            expect(response.users.length).to.eq 1
            user1 = response.users[0]
            expect(user1.links.length).to.eq 1
            expect(user1.links[0].metrics.impressions).to.eq 10
            expect(user1.links[0].metrics.clicks).to.eq 5
            expect(user1.links[0].metrics.destinationClicks).to.eq 5
            done()

      describe 'metric2', ->
        before (done) ->
          timestamp = moment().subtract(2, 'days').startOf('day')
          new Metrics(
            clicks: 5
            impressions: 10
            destinationClicks: 10
            timestamp: timestamp
            user: @user
            link: @link
          ).save().then((metrics) =>
            @link.dailyMetrics.push metrics
            @link.save()
          ).then (link) =>
            done()


        it 'returns a single user with one link with two days', (done) ->
          request(app).get('/api/admin/pendingPayouts').send(
            token: @token
          ).expect(200).end (err, res) =>
            response = res.body
            expect(response.users.length).to.eq 1
            user1 = response.users[0]
            expect(user1.links.length).to.eq 1
            expect(user1.links[0].metrics.impressions).to.eq 20
            expect(user1.links[0].metrics.clicks).to.eq 10
            expect(user1.links[0].metrics.destinationClicks).to.eq 15
            done()

        describe 'link2', ->
          before (done) ->
            short.generate(
              URL: 'http://abc.com/'+Date.now()
              user: @user
            ).then((link) =>
              @link2 = link
              metric = new Metrics(
                timestamp: moment().subtract(1, 'days').startOf('day')
                clicks: 2
                impressions: 2
                destinationClicks: 2
                link: link
                user: @user
              )
              metric.save()
            ).then((metric) =>
              @link2.dailyMetrics.push metric
              @link2.save()
            ).then((link) =>
              User.update({_id:@user._id}, {
                $push: {links: @link2}
              })
            ).then((result) =>
              done()
            )

          it 'returns single user with 2 links', (done) ->
            request(app).get('/api/admin/pendingPayouts').send(
              token: @token
            ).expect(200).end (err, res) =>
              response = res.body
              expect(response.users.length).to.eq 1
              user1 = response.users[0]
              expect(user1.links.length).to.eq 2
              expect(user1.links[0].metrics.impressions).to.eq 20
              expect(user1.links[0].metrics.clicks).to.eq 10
              expect(user1.links[0].metrics.destinationClicks).to.eq 15
              done()


        describe 'second user one link, two metrics', ->
          before (done) ->
            short.generate(
              URL: 'http://abc.com/'+Date.now()
              user: @user2
            ).then((link) =>
              @link2 = link
              metric = new Metrics(
                clicks: 1
                impressions: 2
                destinationClicks: 3
                timestamp: moment().subtract(1, 'days').startOf('day')
                user: @user2
                link: @link2
              )
              metric.save()
            ).then((metric) =>
              @link2.dailyMetrics.push metric
              @link2.save()
            ).then((link) =>
              metric = new Metrics(
                clicks: 2
                impressions: 3
                destinationClicks: 4
                timestamp: moment().subtract(2, 'days').startOf('day')
                user: @user2
                link: @link2
              )
              metric.save()
            ).then((metric) =>
              @link2.dailyMetrics.push metric
              @link2.save()

            ).then((link) =>
              User.update({_id:@user2._id}, {
                $push: {links:link}
                lastAdActivityDate: moment().subtract(1, 'days')
              }).then (result) =>
                done()
            )

          it 'returns two users with links', (done) ->
            request(app).get('/api/admin/pendingPayouts').send(
              token: @token
            ).expect(200).end (err, res) =>
              response = res.body
              expect(response.users.length).to.eq 2
              user1 = response.users[0]
              expect(user1.links.length).to.eq 2
              expect(user1.links[0].metrics.impressions).to.eq 20
              expect(user1.links[0].metrics.clicks).to.eq 10
              expect(user1.links[0].metrics.destinationClicks).to.eq 15

              user2 = response.users[1]
              expect(user2.links.length).to.eq 1
              expect(user2.links[0].metrics.impressions).to.eq 5
              expect(user2.links[0].metrics.clicks).to.eq 3
              expect(user2.links[0].metrics.destinationClicks).to.eq 7
              done()

  describe 'confirmPayouts', ->
    beforeEach (done) ->
      Payout.remove({}).then(() =>

      ).then(() =>
        User.update({}, {balance:0.0}, {multi:true})
      ).then(() =>
        done()
      )

    it 'creates a global payout object', ->
      request(app).post('/api/admin/confirmPayouts').send(
        token: @token
        payouts: [
          {_id: @user._id, payout:1}
        ]
      ).then (res) =>
        Payout.findOne({}).then (payout) =>
          expect(payout.amount).to.eq 1

    it 'confirms one users', ->
      return request(app).post('/api/admin/confirmPayouts').send(
        token: @token
        payouts: [
          {email: @user.email, _id: @user._id, payout: 0.5}
        ]
      ).expect(200).then (res) =>
        response = res.body
        User.findOne(
          _id: @user._id
        ).populate('payouts').exec().then (user) ->
          expect(user.payouts.length).to.eq 1
          payout = user.payouts[0]
          expect(payout.amount).to.eq 0.5
          expect(payout.currency).to.eq 'dollars'
          expect(user.balance).to.eq 0.5

    it 'confirms two users', ->
      return request(app).post('/api/admin/confirmPayouts').send(
        token: @token
        payouts: [
          {email: @user.email, _id: @user._id, payout: 0.5}
          {email: @user2.email, _id: @user2._id, payout: 1.5}
        ]
      ).expect(200).then (res) =>
        response = res.body
        User.find(
          _id: {$in: [@user._id, @user2._id]}
        ).populate('payouts').exec().then (users) ->
          user = users[0]
          expect(user.payouts.length).to.eq 1
          payout = user.payouts[0]
          expect(payout.amount).to.eq 0.5
          expect(payout.currency).to.eq 'dollars'
          expect(user.balance).to.eq 0.5

          user2 = users[1]
          expect(user2.payouts.length).to.eq 1
          payout = user2.payouts[0]
          expect(payout.amount).to.eq 1.5
          expect(user2.balance).to.eq 1.5

    it 'handles a user with a candidate', ->
      @user.candidate = 'trump'
      @user.balance = 0
      @user2.candidate = 'sanders'
      @user2.balance = 0

      @user.save().then( =>
        @user2.save()
      ).then( =>
        request(app).post('/api/admin/confirmPayouts').send(
          token: @token
          payouts: [
            {email: @user.email, _id: @user._id, payout: 0.5}
            {email: @user2.email, _id: @user2._id, payout: 1.5}
          ]
        ).expect(200)
      ).then((res) =>
        body = res.body
        expect(body.success).to.be.true
        User.find(
          _id: {$in: [@user._id, @user2._id]}
        ).populate('payouts').exec()
      ).then((users) =>
        user1 = users[0]
        expect(user1.balance).to.eq 0
        expect(user1.candidate).to.eq 'trump'
        expect(user1.payouts[0].candidate).to.eq 'trump'
        expect(user1.payouts[0].amount).to.eq 0.5

        user2 = users[1]
        expect(user2.balance).to.eq 0
        expect(user2.candidate).to.eq 'sanders'
        expect(user2.payouts[0].candidate).to.eq 'sanders'
        expect(user2.payouts[0].amount).to.eq 1.5
      )



