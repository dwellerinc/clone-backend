config = require '../config'
jwt = require 'jsonwebtoken'
User = require '../models/user'
Promise = require 'bluebird'

exports.getToken = ->
  email = 'email' + Date.now()
  password = 'password'

  return new Promise (resolve, reject) ->
    User.findOne({
      email: email
    }, (err, user) ->
      if not user
        user = new User(
          email: email
          password: password
        )

        user.save (err) ->
          token = jwt.sign(user.toJSON(), config.secret, {})
          resolve({token, user})
      else
        token = jwt.sign(user.toJSON(), config.secret, {})
        resolve({token, user})
    )

