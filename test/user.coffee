util = require '../util/user'
User = require '../models/user'
helpers = require './helpers'
request = require 'supertest-as-promised'
app = require '../server'
UserUtils = require '../util/user'
bcrypt = require 'bcryptjs'
Payout = require '../models/payout'
{ getUserResponse } = require '../util/user'

describe 'user', ->
  before ->
    User.remove({})
    @sandbox = sinon.sandbox.create()
    @sandbox.stub(bcrypt, 'compare').yields(null, true)
    @sandbox.stub(bcrypt, 'genSalt').yields(null, 'salt')
    @sandbox.stub(bcrypt, 'hash').yields(null, 'hash')

  after: ->
    @sandbox.restore()


  describe 'password encryption', ->
    it 'hashes a password', ->
      util.hashPassword('password').then (hash) =>
        expect(hash.length).to.be > 10
        expect(hash).to.not.equal 'password'

    it 'checks the password match', ->
      util.hashPassword('password').then (hash) =>
        util.checkPassword('password', hash).then (res) =>
          expect(res).to.be.true

  describe 'password strength', ->
    it 'checks the strength of a password', ->
      expect(UserUtils.checkPasswordStrength('password', ['gmuresan3@gmail.com'])).to.be.false
      expect(UserUtils.checkPasswordStrength('jf089j1039jf290f', ['ajfoij@kaljf.com'])).to.be.true
      expect(UserUtils.checkPasswordStrength('12345', ['gmuresan3@gmail.com'])).to.be.false
      expect(UserUtils.checkPasswordStrength('spot', ['gmuresan3@gmail.com'])).to.be.false
      expect(UserUtils.checkPasswordStrength('aa8179129', ['gmuresan3@gmail.com'])).to.be.true


  describe 'email verification', ->
    before ->
      helpers.getToken().then(({token,user}) =>
        @token = token
        @user = user
      )

    it 'verifies a user correctly', ->
      request(app).post("/user/verify/#{@user.emailToken.toString()}").expect(200).then((res) =>

        User.findOne(
          _id: @user._id
        )
      ).then((user) =>
        expect(user.verified).to.be.true
      )

    it 'returns 404 for malformed token', ->
      request(app).post('/user/verify/ahjfajsofi').expect(404)

    it 'returns 404 for invalid token', ->
      token = @user.emailToken.toString()
      token = "A" + token.substr(1)
      request(app).post("/user/verify/#{token}").expect(404)

  describe '#getUserResponse', ->
    before ->
      helpers.getToken().then(({token,user}) =>
        @token = token
        @user = user

        @user.candidate = 'trump'
        payout1 = new Payout(
          user: @user
          candidate: 'trump'
          amount: 1
          currency: 'dollars'
        )
        payout2 = new Payout(
          user: @user
          candidate: 'trump'
          amount: 2
          currency: 'dollars'
        )
        payout1.save()
        payout2.save()
        @user.payouts.push(payout1)
        @user.payouts.push(payout2)
        @user.save()
      )

    it 'includes total candidate payout', ->
      getUserResponse(@user).then (userResponse) =>
        expect(userResponse.candidate).to.eq 'trump'
        expect(userResponse.totalCandidatePayout).to.eq 3



