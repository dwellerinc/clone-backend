app = require '../server'
request = require 'supertest-as-promised'
helpers = require './helpers'
config = require '../config'
jwt = require 'jsonwebtoken'
UserUtils = require '../util/user'
User = require '../models/user'
bcrypt = require 'bcryptjs'
short = require 'short'
_ = require 'underscore'
moment = require 'moment'

describe 'server', ->
  before ->
    User.remove({}).then =>
      helpers.getToken().then ({token, user}) =>
        @token = token
        @user = user

  beforeEach ->
    @sandbox = sinon.sandbox.create()
    @sandbox.stub(bcrypt, 'compare').yields(null, true)
    @sandbox.stub(bcrypt, 'genSalt').yields(null, 'salt')
    @sandbox.stub(bcrypt, 'hash').yields(null, 'hash')

  afterEach ->
    @sandbox.restore()

  describe '#register', ->
    it 'registers a user correctly', ->
      email = "email" + Date.now() + "@gmail.com"
      request(app).post('/register').send(
        email: email
        password: 'as9f89asf89haf'
        candidate: 'trump'
      ).expect(200).then (res) =>
        expect(res.body.token).to.exist
        expect(res.body.user.email).to.eq email
        expect(res.body.user.id).to.exist
        User.findOne({email:email}).then (fetchedUser) =>
          expect(fetchedUser.candidate).to.eq 'trump'

    describe 'saved urls', ->
      beforeEach ->
        short.generate(
          URL: 'google.com'
        ).then((url) =>
          @url1 = url
        ).then( =>
          short.generate(
            URL: 'yahoo.com'
          )
        ).then((url) =>
          @url2 = url
        )

      it 'sets current user to url', ->
        email = "email" + Date.now() + "@gmail.com"
        request(app).post('/register').send(
          email: email
          password: 'as9f89asf89haf'
          savedUrlIds: [@url1._id, @url2._id]
        ).expect(200).then((res) =>

          userId = res.body.user._id
          ShortURL.find({_id:{$in:[@url1._id, @url2._id]}}).then (urls) ->
            _.each urls, (url) ->
              expect(url.user.toString()).to.eq userId

          User.findOne({_id: userId}).then (user) =>
            expect(user.links.length).to.eq 2
        )

      it 'doesnt add links generated too long ago', ->
        @url1.created_at = moment().subtract(2, 'hours').toDate()

        @url1.save().then(() =>
          email = "email" + Date.now() + "@gmail.com"
          request(app).post('/register').send(
            email: email
            password: 'as9f89asf89haf'
            savedUrlIds: [@url1._id, @url2._id]
          ).expect(200).then((res) =>

            userId = res.body.user._id
            ShortURL.find({_id:{$in:[@url2._id]}}).then (urls) ->
              _.each urls, (url) ->
                expect(url.user?.toString()).to.eq userId

            User.findOne({_id: userId}).then (user) =>
              expect(user.links.length).to.eq 1
          )
        )


      it 'doesnt set user if already set', ->
        email = "email" + Date.now() + "@gmail.com"

        @url1.user = @user
        @url1.save().then (url) =>

          request(app).post('/register').send(
            email: email
            password: 'asfiuhjf0a9sjf'
            savedUrlIds: [@url1._id, @url2._id]
          ).expect(200).then((res) =>

            userId = res.body.user._id
            ShortURL.find({_id:{$in:[@url1._id, @url2._id]}}).then (urls) =>
              _.each urls, (url) =>
                if url._id.toString() == @url1._id.toString()
                  expect(url.user.toString()).to.not.eq userId
                else
                  expect(url.user.toString()).to.eq userId

          )


  describe '#login', ->
    it 'logs in correctly', ->
      request(app).post('/login').send(
        email: @user.email
        password: 'aabbddee123'
      ).expect(200).then (res) =>
        expect(res.body.token).to.exist
        expect(res.body.user.id).to.equal @user._id.toString()
        expect(res.body.user.password).to.not.exist


  describe '#generateLink', ->
    it 'generates a link for anonymous user', ->
      request(app).post('/generateLink').send(
        originalUrl: 'google.com'
      ).expect(200).then (res) =>
        body = res.body
        console.log body
        expect(body.originalUrl).to.eq 'http://google.com'
        short.retrieve(body.hash).then (url) ->
          expect(url.user).to.eq undefined

    it 'generates two separate hashes for two users', ->
      request(app).post('/generateLink').send(
        originalUrl: 'google.com'
      ).expect(200).then (res) =>
        @hash1 = res.body.hash

        request(app).post('/generateLink').send(
          originalUrl: 'google.com'
        ).expect(200).then (res) =>
          @hash2 = res.body.hash

          expect(@hash1).to.not.eq @hash2


