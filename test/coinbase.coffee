helpers = require './helpers'
testRequest = require 'supertest-as-promised'
app = require '../server'
request = require 'request-promise'
config = require '../config'
Bluebird = require 'bluebird'
require('sinon-as-promised')(Bluebird)
User = require '../models/user'
moment = require 'moment'
coinbase = require '../coinbase.coffee'

describe 'coinbase', ->
  before (done) ->
    helpers.getToken().then ({token, user}) =>
      @token = token
      @user = user
      @sandbox = sinon.sandbox.create()
      @postSpy = @sandbox.spy()
      done()

  afterEach ->
    @sandbox.restore()

  describe '#coinbase/url', ->
    it 'returns the coinbase url', ->
      testRequest(app).get('/api/coinbase/url').send(
        redirectUri: 'www'
        token: @token
      ).expect(200).then (res) =>
        expect(res.body.url).to.exist

  describe '#coinbase/confirm', ->
    it 'posts to coinbase', ->
      postSpy = @sandbox.stub(request, 'post').resolves({
        "access_token": "1234"
        "token_type": "bearer"
        "expires_in": 7200
        "refresh_token": "abcd"
        "scope": "wallet:user:email"
      })
      testRequest(app).post('/api/coinbase/confirm').send(
        state: 'random'
        code: 'code'
        token: @token
        redirectUri: 'http://localhost'
      ).expect(200).then (res) =>

        expect(postSpy).to.have.been.calledWith(
          url: config.coinbase.OAuthUrl
          form: {
            grant_type: 'authorization_code'
            code: 'code'
            client_id: config.coinbase.clientId
            client_secret: config.coinbase.secret
            redirect_uri: 'http://localhost'
            scope: 'wallet:user:email'
          }
          json: true
        )
        User.findOne({_id: @user._id}).then (user) =>
          expect(user.coinbase).to.include {
            accessToken: '1234'
            refreshToken: 'abcd'
            scope: 'wallet:user:email'
            expiresIn: 7200
            tokenType: 'bearer'
          }
          expect(user.coinbase.expirationDate).to.be.closeTo(
            moment().add(7200, 'seconds').unix()*1000, 1000)

  describe 'refreshToken', ->
    before ->
      @user.coinbase = {
        refreshToken:"old"
        token_type:"bearer",
        expires_in:7200,
        refresh_token:"abcd",
        scope:"wallet:user:email"
      }
      @user.markModified('coinbase')
      @user.save().then (user) =>
        @user = user

    it 'refreshes coinbase token', ->
      postSpy = @sandbox.stub(request, 'post').resolves({
        "access_token":"new"
        "token_type":"bearer",
        "expires_in":7200,
        "refresh_token":"new_refresh",
        "scope":"all"
      })
      coinbase.refreshToken(@user).then (user) =>
        expect(postSpy).to.have.been.calledWith(
          url: config.coinbase.OAuthUrl
          form:
            grant_type: 'refresh_token'
            refresh_token: 'old'
            client_id: config.coinbase.clientId
            client_secret: config.coinbase.secret
            redirect_uri: 'api/coinbase/url'
          json: true
        )

        User.findOne({_id:@user._id}).then (user) =>
          expect(user.coinbase.accessToken).to.eq 'new'
          expect(user.coinbase.refreshToken).to.eq 'new_refresh'

