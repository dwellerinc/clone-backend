express = require 'express'
UserUtils = require '../util/user'
User = require '../models/user'
querystring = require 'querystring'
config = require '../config'

routes = express.Router()

module.exports = routes

routes.get '/getUser', (req, res) ->
  UserUtils.getUserResponse(req.user).then (response) ->
    res.json {user: response}


routes.post '/updateName', (req, res) ->
  fullName = req.body.fullName
  req.user.fullName = fullName
  req.user.save().then (user) ->
    res.json {sucess: true}


routes.get '/coinbase/url', (req, res) ->
  redirect_uri = req.query.redirectUri or req.body.redirectUri

  query = querystring.stringify(
    response_type: 'code'
    client_id: config.coinbase.clientId
    redirect_uri: redirect_uri
    state: 'random'
    scope: 'wallet:accounts:read,wallet:user:email'
  )

  url = "#{config.coinbase.authUrl}?#{query}"

  res.json {url: url}


routes.post '/changePassword', (req, res) ->
  User.update(
    { _id: req.user._id }
    { $set: { password: req.body.password } }
  ).then (user) ->


