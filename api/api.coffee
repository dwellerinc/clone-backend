config = require '../config'
secret = config.secret
express = require 'express'
jwt = require 'jsonwebtoken'
short = require 'short'
User = require '../models/user'
normalizeUrl = require 'normalize-url'
{ ShortURL } = require 'short'
util = require '../util'
request = require 'request-promise'
_ = require 'underscore'
adminRoutes = require '../admin'
coinbase = require '../coinbase'
Promise = require 'bluebird'
moment = require 'moment'
UrlCrawler = require '../url_crawler'
UserUtils = require '../util/user.coffee'

candidatesApi = require './user/candidate'
userApi = require './user'

apiRoutes = express.Router()

apiRoutes.use (req, res, next) ->
  token = req.body.token or req.query.token or req.headers['x-access-token']

  if token
    jwt.verify token, secret, (err, decoded) ->
      if err
        return res.status(401).json {success: false, message: 'Auth token failed'}
      else
        User.findOne({
          _id: decoded._id
        }, (err, user) ->
          if err then throw new Error(err)
          if not user
            res.send(401)
            return
          req.user = user
          req.decoded = decoded
          next()
        )
  else
    res.status(401).json {success: false, message: "No Auth Token"}

apiRoutes.use '/admin', adminRoutes
apiRoutes.use '/', userApi
apiRoutes.use '/user', candidatesApi


apiRoutes.post '/coinbase/confirm', (req, res) ->
  state = req.body.state
  code = req.body.code
  redirectUri = req.body.redirectUri

  request.post(
    url: config.coinbase.OAuthUrl,
    form:{
      grant_type: 'authorization_code'
      code: code
      client_id: config.coinbase.clientId
      client_secret: config.coinbase.secret
      redirect_uri: redirectUri
      scope: 'wallet:user:email'
    }
    json: true
  ).then((response) ->
    coinbase.handleCoinbaseAuthResponse(req.user, response).then((user)->
      coinbase.populateCoinbaseAccount(user)
    ).then((user) ->
      res.json {success: true}
    )
  ).catch (err) ->
    console.log err
    res.json {success: false}

apiRoutes.post '/coinbase/deauthorize', (req, res) ->
  req.user.coinbase = {}
  req.user.markModified('coinbase')
  req.user.save().then( ->
    res.json {success: true}
  )

apiRoutes.post '/generateLink', (req, res) ->
  originalUrl = normalizeUrl(req.body.originalUrl)

  UrlCrawler.validateUrl(originalUrl).then((success) ->
    if not success
      return res.json {success: false, error: "Bad URL"}

    short.generate(
      URL: originalUrl
      user: req.user
    ).then((mongodbDoc) ->
      hash = mongodbDoc.hash
      shortUrl = config.shortUrl + hash

      User.update(
        { _id: req.user._id }
        { $addToSet: {links: mongodbDoc._id} }
      ).then (result) ->
        User.find({_id: req.user._id}).then (user) ->
          UrlCrawler.populateHeadElements(mongodbDoc)
          res.json {hash: hash, originalUrl: originalUrl, shortUrl: shortUrl}
      , (err) ->
        console.log err
        thow new Error(err)

    , (error) ->
      console.log error
      if error then throw new Error(error)
    )
  )

apiRoutes.get '/getLinks', (req, res) ->
  ShortURL.find(
    _id: { $in: req.user.links}
  ).populate('totalMetrics').sort(created_at:'descending').exec().then (urls) ->
    urlsJson = urls.map (url) ->
      titleElement = _.find url.headElements, (el) ->
        return el.tag == 'title'
      title = titleElement?.innerHtml
      return {
        originalUrl: url.URL
        shortUrl: config.shortUrl + url.hash
        clicks: url.totalMetrics.clicks
        impressions: url.totalMetrics.impressions
        destinationClicks: url.totalMetrics.destinationClicks
        date: url.created_at
        title: title
      }

    res.json {links: urlsJson}


apiRoutes.post '/withdraw', (req, res) ->
  amount = req.body.amount
  if not req.user?.coinbase.refreshToken
    res.json {success: false, error: "Coinbase account not connected"}
    return

  if amount > req.user.balance
    resj.json {success: false, error: "Can't withdraw more than amount"}

  expirationCushion = moment(req.user.coinbase.expirationDate).subtract(5, 'seconds').toDate().getTime()
  if moment() > expirationCushion
    waitFor = coinbase.refreshToken(req.user)
  else
    waitFor = Promise.resolve(req.user)

  waitFor.then((user)->
    coinbase.withdrawBitcoin(user, amount)
  ).then((txn) ->
    console.log txn
    if txn.status == 'completed'
      req.user.balance -= amount
      req.user.save()
  ).then(->
    res.json {success: true}
  ).catch((error) ->
    console.log error
    res.json {success: false}
  )

module.exports = apiRoutes

