express = require 'express'

routes = express.Router()
module.exports = routes

VALID_CANDIDATES = [null, 'trump', 'cruz', 'kasich', 'carson', 'rubio', 'sanders', 'clinton']

routes.post '/setCandidate', (req, res) ->
  candidate = req.body.candidate

  if candidate not in VALID_CANDIDATES
    return res.json {success: false, error: 'Invalid candidate selected'}

  req.user.candidate = candidate
  req.user.save().then ->
    return res.json {success: true, candidate:candidate}

