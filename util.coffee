URL = require 'url'
Payout = require './models/payout'

exports.currentBaseUrl = (req) ->
  URL.format(
    protocol: req.protocol
    host: req.get('host')
  )

exports.currentFullUrl = (req) ->
  URL.format(
    protocol: req.protocol,
    host: req.get('host')
    pathname: req.originalUrl
  )

exports.getLastPayout = ->
  Payout.findOne({user: null}, {}, {sort: {date:-1}})

