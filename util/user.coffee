fs = require 'fs'
bcrypt = require 'bcryptjs'
Promise = require 'bluebird'
_ = require 'underscore'
nodemailer = require 'nodemailer'
config = require '../config'
xoauth2 = require 'xoauth2'
jwt = require 'jsonwebtoken'
passwordStrength = require 'zxcvbn'
Payout = require '../models/payout'
{ ShortURL } = require 'short'
moment = require 'moment'

EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

verificationTemplate = fs.readFileSync('./templates/email/signup.html', encoding='utf8')
passwordResetTemplate = fs.readFileSync('./templates/email/reset_password.html', encoding='utf8')

UserUtils = {

  generateJwt: (json) ->
    jwt.sign(json, config.secret, {})

  checkPasswordStrength: (password, userInputs=[]) ->
    strength = passwordStrength(password, userInputs)
    return strength.score >= 2

  hashPassword: (password) ->
    return new Promise (resolve, reject) ->
      bcrypt.genSalt 10, (err, salt) ->
        if err
          reject(err)

        bcrypt.hash password, salt, (err, hash) ->
          if err
            reject(err)
          resolve(hash)


  checkPassword: (password, hash) ->
    return new Promise (resolve, reject) ->
      bcrypt.compare password, hash, (err, res) ->
        resolve(res)


  populateSavedUrls: (user, savedUrlIds) ->
    ShortURL.find({_id: {$in: savedUrlIds}}).then (urls) ->
      urls = _.filter urls, (url) ->
        if url.user?
          return false

        oneHourAgo = moment().subtract(1, 'hours')
        if url.created_at < oneHourAgo
          return false

        return true

      promises = []
      _.each urls, (url) ->
        url.user = user
        user.links.push(url)
        promises.push(url.save())

      promises.push(user.save())

      return Promise.all(promises)


  getUserResponse: (user) ->
    userResponse = user.toJSON()
    userResponse = _.omit userResponse, ['coinbase', 'password']
    userResponse.id = user._id

    if user.coinbase?.accessToken
      userResponse.coinbase = {}
      userResponse.coinbase.dateConnected = user.coinbase.dateConnected
      userResponse.coinbase.name = user.coinbase.name

    return UserUtils.getCandidateTotal(user).then (candidatePayout) ->
      userResponse.totalCandidatePayout = candidatePayout

      return userResponse


  getCandidateTotal: (user) ->
    candidate = user.candidate

    if candidate
      promise = Payout.find({_id:{$in:user.payouts}}).then (payouts) ->
        payouts = _.filter payouts, (payout) ->
          payout.candidate == user.candidate


        candidatePayout = _.reduce(payouts, (sum, payout) ->
          return sum + payout.amount
        , 0)
        return candidatePayout

    else
      promise = Promise.resolve(0)

    return promise


  validateEmail: (email) ->
    return email.match EMAIL_REGEX


  sendVerificationEmail: (user) ->
    transporter = getEmailTransporter()

    data = {
      verificationUrl: "https://truvi.co/account/verify/#{user.emailToken.toString()}"
    }

    compiled = _.template verificationTemplate
    html = compiled(data)

    mailOptions = {
      from: config.gmail
      to: user.email
      subject: 'Truvi Account Verification'
      html: html
    }

    return transporter.sendMail(mailOptions, (error, info) ->
      console.log "Registration email sent"
    )

  sendPasswordResetEmail: (user) ->
    transporter = getEmailTransporter()
    data = {
      resetPasswordUrl: "https://truvi.co/account/reset_password/#{user.passwordToken}"
      name: user.fullName
    }

    compiled = _.template passwordResetTemplate
    html = compiled(data)

    mailOptions = {
      from: config.email
      to: user.email
      subject: 'Truvi Password Reset'
      html: html
    }

    transporter.sendMail(mailOptions, (err, info) ->
      console.log "Message Sent"
      console.log err
      console.log info
    )

}

module.exports = UserUtils

getEmailTransporter = ->
  return nodemailer.createTransport(
    service: 'gmail'
    auth:
      xoauth2: xoauth2.createXOAuth2Generator({
        user: config.gmail.email
        clientId: config.gmail.clientId
        clientSecret: config.gmail.clientSecret
        refreshToken: config.gmail.refreshToken
        accessToken: config.gmail.accessToken
      })
  )
