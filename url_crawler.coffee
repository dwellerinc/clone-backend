config = require './config'
express = require 'express'

Promise = require 'bluebird'
request = require 'request-promise'
cheerio = require 'cheerio'

{ HtmlElement, HtmlAttribute } = require 'short'
_ = require 'underscore'

ALLOWED_HEAD_ELEMENTS = {
  meta: ['description', 'bf:buzzid', 'author', 'news_keywords', 'sailthru.tags', 'fb:app_id', 'og:site_name',
         'og:title', 'og:description', 'og:url', 'article:tag', 'article:section', 'article:publisher',
         'og:image', 'og:image_width', 'og:image_height', 'og:type', 'twitter:card', 'twitter:title',
         'twitter:description', 'twitter:image', 'twitter:site', 'twitter:url', 'twitter:title',
         'twitter:app:id:iphone', 'twitter:app:url:iphone', 'twitter:app:id:ipad', 'twitter:app:url:ipad',
         'twitter:app:id:googleplay', 'twitter:app:url:googleplay', 'twitter:creator', 'twitter:creator:id'
  ]
  title: ['']
  link: ['canonical', 'alternate','image_src', 'apple-touch-startup-image', 'apple-touch-icon', 'shortcut icon']


}

UrlCrawler = {

  populateHeadElements: (urlObject) ->
    request.get(
      url: urlObject.URL
      followAllRedirects: true
      resolveWithFullResponse: true
      maxRedirects: 20
    ).then((response) ->
      $ = cheerio.load response.body
      headChildren = $('head').children()
      headElements = _.map headChildren , (el) ->
        tag = el.name
        name = el.attribs.name or ''
        content = el.attribs.content
        property = el.attribs.property
        innerHtml = el.children?[0]?.data
        rel = el.attribs.rel

        if name? and name != '' then key=name else key=rel

        if name
          key = name
        else if rel
          key = rel
        else if property
          key = property
        else
          key = ''

        allowedKeys = ALLOWED_HEAD_ELEMENTS[tag]
        if allowedKeys and (key in allowedKeys)
          attributes = _.map el.attribs, (val, key) ->
            if val[0] == '/'
              val = "#{response.request.uri.protocol}//#{response.request.uri.host}#{val}"
            return new HtmlAttribute(
              key: key
              value: val
            )

          htmlElement = new HtmlElement(
            tag: tag
            attributes: attributes
            innerHtml: innerHtml
          )

          return htmlElement

      headElements = _.filter headElements, (el) ->
        return el?

      urlObject.headElements = headElements

      headAttributes = _.map $('head')['0'].attribs, (val, key) ->
        return new HtmlAttribute(
          key: key
          value: val
        )

      urlObject.headAttributes = headAttributes

      return urlObject.save()
    )


  validateUrl: (url) ->
    request.get(
      url: url
      followAllRedirects: true
      resolveWithFullResponse: true
      simple: false
      maxRedirects: 20
    ).then((response) ->
      return true
    ).catch((error) ->
      return false
    )

}

module.exports = UrlCrawler

