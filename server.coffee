if process.env.NODE_ENV == 'debug'
  require('dotenv').config()

require('events').EventEmitter.prototype._maxListeners = 100
#emitter.setMaxListeners(100)

require 'newrelic'
http = require('http')
express = require('express')
bodyParser = require('body-parser')
app = express()
cors = require 'cors'
_ = require 'underscore'
db = require './models/db'
User = require './models/user'
morgan = require 'morgan'
config = require './config'
apiRoutes = require './api/api'
UserUtils = require './util/user.coffee'
normalizeUrl = require 'normalize-url'
UrlCrawler = require './url_crawler'
ObjectId = require('mongoose').Types.ObjectId

short = require 'short'

short.connect(config.database)
short.connection.on 'error', (error) ->
  throw new Error(error)

server = http.Server(app)

app.use bodyParser.urlencoded(extended: false)
app.use(bodyParser.json())
app.use(cors())

if process.env.NODE_ENV != 'test'
  app.use(morgan('dev'))

app.get '/', (req, res) ->
  res.send('Index')

app.post '/login', (req, res) ->
  email = req.body.email.toLowerCase()
  password = req.body.password

  User.findOne({
    email: email,
  }, (err, user) ->
    if err then throw err

    if not user
      res.json {success: false, error: 'Invalid username and password'}
    else if user
      UserUtils.checkPassword(password, user.password).then (success) ->
        if not success
          res.json {success: false, error: 'Invalid username and password'}
        else
          UserUtils.getUserResponse(user).then (userResponse) ->
            token = UserUtils.generateJwt(userResponse)

            res.json({
              user: userResponse,
              token: token
            })
    )


app.post '/register', (req, res) ->
  email = req.body.email.toLowerCase()
  password = req.body.password
  fullName = req.body.fullName
  candidate = req.body.candidate
  savedUrlIds = req.body.savedUrlIds

  if not UserUtils.validateEmail(email)
    return res.json {success: false, error: 'Please use a valid email'}

  if not UserUtils.checkPasswordStrength(password, [email, fullName])
    return res.json {success: false, error: 'Please use a stronger password'}

  UserUtils.hashPassword(password).then (hash) ->
    User.findOne({
      email: email
    }, (err, user) ->
      if err then throw err

      if user
        res.json {success: false, error: 'Email already taken'}
      else
        newUser = new User(
          email: email
          password: hash
          fullName: fullName
          candidate: candidate
          admin: false
        )

        newUser.save (err) ->

          if err then throw err
          UserUtils.sendVerificationEmail(newUser)

          UserUtils.populateSavedUrls(newUser, savedUrlIds).then ->
            UserUtils.getUserResponse(newUser).then (userResponse) ->
              token = UserUtils.generateJwt(userResponse)
              res.json {success: true, token: token, user: userResponse}
    )


app.post '/generateLink', (req, res) ->
  originalUrl = normalizeUrl(req.body.originalUrl)

  UrlCrawler.validateUrl(originalUrl).then((success) ->
    if not success then return res.json {success: false, error: 'Invalid URL'}

    short.generate(
      URL: originalUrl
    ).then((shortUrlObject) ->
      hash = shortUrlObject.hash
      shortUrl = config.shortUrl + hash
      UrlCrawler.populateHeadElements(shortUrlObject)
      return res.json {
        hash: hash,
        originalUrl: originalUrl,
        shortUrl: shortUrl
        id: shortUrlObject._id
      }
    )
  )

app.post '/user/verify/:token', (req, res) ->
  token = req.params.token

  User.findOne(
    emailToken: token
  ).then((user) ->
    if user
      user.verified = true
      user.emailToken = null
      user.save().then (user) ->
        UserUtils.getUserResponse(user).then (userResponse) ->
          token = UserUtils.generateJwt(userResponse)
          res.json {success: true, user:userResponse, token:token}
    else
      res.status(404).json {success: false}
  ).catch (e) ->
      res.status(404).json {success: false}

app.post '/user/resetPassword', (req, res) ->
  email = req.body.email

  User.findOne(
    email: email
  ).then((user) ->

    if user
      user.passwordToken = new ObjectId()
      user.save().then (user) ->
        UserUtils.sendPasswordResetEmail(user)
        res.json {success: true}
    else
      res.json {success: false}
  )

app.post '/user/setPassword', (req, res) ->
  password = req.body.password
  token = req.body.token

  if not UserUtils.checkPasswordStrength(password)
    return res.json {success: false, error: 'Please use a stronger password'}

  User.findOne(
    passwordToken: token
  ).then (user) ->
    if user
      UserUtils.hashPassword(password).then (hash) ->
        user.password = hash
        user.passwordToken = null
        user.save().then (user) ->
          res.json {success: true}
    else
      res.json {success: false, error: 'User not found'}

app.use '/api', apiRoutes

# Launch the HTTP server serving the Express app
server.listen process.env.PORT or 3001

console.log 'Starting server'
console.log "Port: #{process.env.PORT}"

module.exports = app

