config = require '../config'
mongoose = require 'mongoose'

mongoose.connect(config.database)
mongoose.Promise = require 'bluebird'

