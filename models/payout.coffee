mongoose = require 'mongoose'
Schema = mongoose.Schema
ObjectId = Schema.ObjectId

module.exports = mongoose.model('Payout', new Schema(
  date: {type: Date, default: Date.now}
  currency: String,
  amount: Number
  user: {type: ObjectId, ref:'User'}
  candidate: {type: String, index: true}
))


