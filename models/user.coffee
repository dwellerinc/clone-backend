mongoose = require 'mongoose'
Schema = mongoose.Schema
ObjectId = Schema.ObjectId
Payout = require './payout'

module.exports = mongoose.model('User', new Schema(
  firstName: String,
  lastName: String,
  fullName: String,
  email: {type: String, unique: true, index:true}
  password: {type: String, unique: false}
  admin: {type: Boolean, default: false}
  candidate: {type: String, index: true}

  lastAdActivityDate: Date,
  lastPayoutDate: Date,
  links: [{ type: ObjectId, ref: 'ShortURL', default:[]}]
  coinbase: Schema.Types.Mixed

  payouts: [{type: ObjectId, ref:'Payout'}]
  balance: {type: Number, default: 0.0}

  emailToken: {type: ObjectId, unique: true, index: true, default: mongoose.Types.ObjectId}
  passwordToken: {type: ObjectId, unique:true, index: true, sparse: true}
  verified: {type: Boolean, default: false}

))


