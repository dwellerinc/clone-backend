
database = process.env.MONGOLAB_URI
shortUrl = process.env.SHORT_URL or 'http://trymeal.com/'

dollarsPerKImpressions = 1.11

coinbaseAuthUrl = process.env.COINBASE_AUTH_URL or 'https://sandbox.coinbase.com/oauth/authorize'
coinbaseClientId = process.env.COINBASE_CLIENT_ID or '2f89bd31b30c763516ad13f84fdac0b9e0f5771501ed0ddaa0c78d710b64486a'
coinbaseOAuthUrl = process.env.COINBASE_OAUTH_URL or 'https://api.sandbox.coinbase.com/oauth/token'
coinbaseBaseApiUrl = process.env.COINBASE_API_URL or 'https://api.sandbox.coinbase.com/v2/'
coinbaseClientSecret = process.env.COINBASE_CLIENT_SECRET or '5dce3f6f81969f48b2d3b9c1af7fd783e6ff6192bcd702844ca8ee0eb608bb1f'
coinbaseApiKey = process.env.COINBASE_API_KEY or 'NAWsQy7MYU8w6BX3'
coinbaseApiSecret = process.env.COINBASE_API_SECRET or 'H0pUR1JZon7vbuTnLIaOyS68Du6QTpy6'
secret = process.env.SECRET_KEY or 'u80u1f098j2809hf80h30h'

gmailEmail = process.env.GMAIL_EMAIL
gmailClientId = process.env.GMAIL_CLIENT_ID
gmailClientSecret = process.env.GMAIL_CLIENT_SECRET
gmailRefreshToken = process.env.GMAIL_REFRESH_TOKEN
gmailAccessToken = process.env.GMAIL_ACCESS_TOKEN

if process.env.NODE_ENV == 'test'
  database = 'mongodb://localhost/clone-test'
  shortUrl = 'http://localhost:3000/'

if process.env.NODE_ENV == 'debug'
  database = 'mongodb://localhost/clone'
  shortUrl = 'http://localhost:3000/'

module.exports = {
  secret: secret
  database: database
  shortUrl: shortUrl
  gmail: {
    email: gmailEmail
    clientId: gmailClientId
    clientSecret: gmailClientSecret
    refreshToken: gmailRefreshToken
    accessToken: gmailAccessToken
  }

  coinbase: {
    authUrl: coinbaseAuthUrl
    clientId: coinbaseClientId
    OAuthUrl: coinbaseOAuthUrl
    secret: coinbaseClientSecret
    apiKey: coinbaseApiKey
    apiSecret: coinbaseApiSecret
    baseApiUrl: coinbaseBaseApiUrl
  }
  dollarsPerKImpressions: dollarsPerKImpressions
}
