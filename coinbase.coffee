request = require 'request-promise'
config = require './config'
moment = require 'moment'
Client = require('coinbase').Client
Promise = require 'bluebird'
Promise.promisifyAll(Client.prototype)
Promise.promisifyAll(require('coinbase').model.Account.prototype)
_ = require 'underscore'

client = new Client(
  'apiKey': config.coinbase.apiKey
  'apiSecret': config.coinbase.apiSecret
  'baseApiUri': config.coinbase.baseApiUrl,
  'tokenUri': config.coinbase.OAuthUrl
)

primaryAccount = null
client.getAccountsAsync({}).then((accounts) ->
  primaryAccount = _.find accounts, (account) ->
    account.primary == true
)

exports.getClient = (user) ->
  return new Client(
    'accessToken': user.coinbase.accessToken,
    'refreshToken': user.coinbase.refreshToken
    'baseApiUri': config.coinbase.baseApiUrl,
    'tokenUri': config.coinbase.OAuthUrl
  )

exports.refreshToken = (user) ->
  request.post(
    url: config.coinbase.OAuthUrl
    form: {
      grant_type: 'refresh_token'
      redirect_uri:'api/coinbase/url'
      client_id: config.coinbase.clientId
      client_secret: config.coinbase.secret
      refresh_token: user.coinbase.refreshToken
    }
    json: true
  ).then (response) ->
    exports.handleCoinbaseAuthResponse(user, response)


exports.withdrawBitcoin = (user, dollars) ->
  args =
    to: user.coinbase.email
    amount: dollars.toFixed(2)
    currency: 'USD'
    description: 'Truvi withdrawal'

  primaryAccount.sendMoneyAsync(args)

exports.populateCoinbaseAccount = (user) ->
  if user.coinbase.accessToken?
    client = exports.getClient(user)
    client.getCurrentUserAsync().then (result) ->
      console.log result
      user.coinbase.email = result.email
      user.coinbase.name = result.name
      if not user.fullName
        user.fullName = result.name
      user.markModified('coinbase')
      console.log user
      user.save()

exports.handleCoinbaseAuthResponse = (user, response) ->
  coinbase = {}
  coinbase.accessToken = response.access_token
  coinbase.expirationDate = moment().add(response.expires_in, 'seconds')
  coinbase.refreshToken = response.refresh_token
  coinbase.scope = response.scope
  coinbase.expiresIn = response.expires_in
  coinbase.tokenType = response.token_type
  coinbase.dateConnected = Date.now()
  user.coinbase = coinbase
  return user.save()


